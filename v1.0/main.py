import Round as Round

import time
import pygame


class GUI:
    # Метод прорисовывает полную картнку
    def drawAllPictures(self, selectedRound, window, listOfSlices):
        position = [200, 130]
        window.blit(listOfSlices[0], position)

        return True

    # Метод отрисовывает текущее положение частичек
    # На вход получает:
    # - объект Round
    # - окно из pygame
    # - список прогруженных частичек (формируется loadSelectedPictures)
    def drawSelectedLocationPictures(self, selectedRound, window, listOfSlices):
        position = [120, 20]
        for row in selectedRound.slicesLocation:
            for currentSlice in row:
                if (currentSlice != 0):
                    window.blit(listOfSlices[currentSlice], position)
                position[0] = position[0] + 115
            position[0] = 120
            position[1] = position[1] + 115

        return True

    # Метод подгружает частички выбранной картинки
    # на вход получает объект Round
    def loadSelectedPictures(self, selectedRound):
        folderPath = selectedRound.picture
        listOfSlices = list()

        # Загружаем все картинки
        slice1 = pygame.image.load(folderPath + "/1.jpg").convert()
        slice2 = pygame.image.load(folderPath + "/2.jpg").convert()
        slice3 = pygame.image.load(folderPath + "/3.jpg").convert()
        slice4 = pygame.image.load(folderPath + "/4.jpg").convert()
        slice5 = pygame.image.load(folderPath + "/5.jpg").convert()
        slice6 = pygame.image.load(folderPath + "/6.jpg").convert()
        slice7 = pygame.image.load(folderPath + "/7.jpg").convert()
        slice8 = pygame.image.load(folderPath + "/8.jpg").convert()
        slice9 = pygame.image.load(folderPath + "/9.jpg").convert()
        slice10 = pygame.image.load(folderPath + "/10.jpg").convert()
        slice11 = pygame.image.load(folderPath + "/11.jpg").convert()
        slice12 = pygame.image.load(folderPath + "/12.jpg").convert()
        slice13 = pygame.image.load(folderPath + "/13.jpg").convert()
        slice14 = pygame.image.load(folderPath + "/14.jpg").convert()
        slice15 = pygame.image.load(folderPath + "/15.jpg").convert()
        sliceAll = pygame.image.load(folderPath + "/main.jpg").convert()

        # Сохраняем картинки в список для дальнейшей работы
        listOfSlices.append(sliceAll)
        listOfSlices.append(slice1)
        listOfSlices.append(slice2)
        listOfSlices.append(slice3)
        listOfSlices.append(slice4)
        listOfSlices.append(slice5)
        listOfSlices.append(slice6)
        listOfSlices.append(slice7)
        listOfSlices.append(slice8)
        listOfSlices.append(slice9)
        listOfSlices.append(slice10)
        listOfSlices.append(slice11)
        listOfSlices.append(slice12)
        listOfSlices.append(slice13)
        listOfSlices.append(slice14)
        listOfSlices.append(slice15)

        return listOfSlices

    def startApp(self):
        pygame.init()

        screenSize = (700, 500)

        isRuning = True

        inGame = False

        # Задаём размер окна
        pygame.display.set_caption("Puzzle Game")

        # Задаём иконку в окне
        icon = pygame.image.load("system/ICO.png")
        pygame.display.set_icon(icon)

        # Показываем главное окно
        mainWindowGame = pygame.display.set_mode(screenSize)

        # Прогружаем фоное изображение
        bgImage = pygame.image.load("system/BACKGROUND.jpg").convert()
        bgImageInfo = pygame.image.load("system/BACKGROUND_INFO.jpg").convert()
        if (not inGame):
            mainWindowGame.blit(bgImageInfo, (0, 0))
        else:
            mainWindowGame.blit(bgImage, (0, 0))

        # Прогружаем и проигрываем фоновую музыку
        pygame.mixer.music.load("BACKGROUND_MUSIC.mp3")
        pygame.mixer.music.play(-1)
        sound_moving_block = pygame.mixer.Sound("SYSTEM_moving_block.ogg")

        # Прозрачный курсор
        pygame.mouse.set_visible(False)

        while isRuning:
            # Обновление не чаще 25 раз в секунду
            pygame.time.delay(40)

            # Если событие QUIT - выйти
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    isRuning = False

            # Выйти по нажатию ESC
            if pygame.key.get_pressed()[pygame.K_ESCAPE]:
                pygame.event.post(pygame.event.Event(pygame.QUIT))

            # Инициализируемпеременную isCorrectOrder
            isCorrectOrder = False

            # Начать игру по нажатию SPACE
            # По сути, основной метод
            if ((pygame.key.get_pressed()[pygame.K_SPACE]) & (not inGame)):
                inGame = True
                mainWindowGame.blit(bgImage, (0, 0))

                currentRound = Round.Round("MATH/")
                listOfSlices = self.loadSelectedPictures(currentRound)
                self.drawSelectedLocationPictures(currentRound, mainWindowGame, listOfSlices)
                isCorrectOrder = currentRound.isCorrectOrder()

                pygame.display.update()

                pygame.time.delay(40)

            # По нажатию BACKSPACE выход из игры в меню
            if (pygame.key.get_pressed()[pygame.K_BACKSPACE] & inGame):
                inGame = False
                if (not inGame):
                    mainWindowGame.blit(bgImageInfo, (0, 0))
                else:
                    mainWindowGame.blit(bgImage, (0, 0))
                inGame = False
                isCorrectOrder = True

            # По нажатию ВНИЗ движение на пустое место
            if ((pygame.key.get_pressed()[pygame.K_DOWN]) & (not isCorrectOrder)):
                currentRound.moveUp()
                mainWindowGame.blit(bgImage, (0, 0))
                self.drawSelectedLocationPictures(currentRound, mainWindowGame, listOfSlices)
                isCorrectOrder = currentRound.isCorrectOrder()
                sound_moving_block.play()
                time.sleep(0.5)

            # По нажатию ВВЕРХ движение на пустое место
            if ((pygame.key.get_pressed()[pygame.K_UP]) & (not isCorrectOrder)):
                currentRound.moveDown()
                mainWindowGame.blit(bgImage, (0, 0))
                self.drawSelectedLocationPictures(currentRound, mainWindowGame, listOfSlices)
                isCorrectOrder = currentRound.isCorrectOrder()
                sound_moving_block.play()
                time.sleep(0.5)

            # По нажатию ВПРАВО движение на пустое место
            if ((pygame.key.get_pressed()[pygame.K_RIGHT]) & (not isCorrectOrder)):
                currentRound.moveLeft()
                mainWindowGame.blit(bgImage, (0, 0))
                self.drawSelectedLocationPictures(currentRound, mainWindowGame, listOfSlices)
                isCorrectOrder = currentRound.isCorrectOrder()
                sound_moving_block.play()
                time.sleep(0.5)

            # По нажатию ВЛЕВО движение на пустое место
            if ((pygame.key.get_pressed()[pygame.K_LEFT]) & (not isCorrectOrder)):
                currentRound.moveRight()
                mainWindowGame.blit(bgImage, (0, 0))
                self.drawSelectedLocationPictures(currentRound, mainWindowGame, listOfSlices)
                isCorrectOrder = currentRound.isCorrectOrder()
                sound_moving_block.play()
                time.sleep(0.5)

            # Если частички уже по местам - конец раунда
            if ((isCorrectOrder) & (inGame)):
                isCorrectOrder = False
                mainWindowGame.blit(bgImage, (0, 0))
                self.drawAllPictures(currentRound, mainWindowGame, listOfSlices)
                times_new_roman = pygame.font.match_font("times new roman")
                font = pygame.font.Font(times_new_roman, 40)
                textCONGRATULATIONS = font.render("CONGRATULATIONS !!! YOU WIN!", True, [255, 255, 255])
                mainWindowGame.blit(textCONGRATULATIONS, [45, 50])

                times_new_roman = pygame.font.match_font("times new roman")
                font = pygame.font.Font(times_new_roman, 25)
                textINFO = font.render("Press BACKSPACE to exit the menu", True, [255, 255, 255])
                mainWindowGame.blit(textINFO, [160, 450])

            pygame.display.update()

        pygame.quit()


gui = GUI()
gui.startApp()
