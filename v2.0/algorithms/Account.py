class Account:
    # Список всех доступных картинок
    allPictureList = list()
    
    # Список сложенных картинок
    foldedPictureList = list()
    
    # Список открытых картинок
    openedPictureList = list()
    
    # Текущее количество очков
    points = 0
    
    # Настройки звука
    withSound = True
    withMisic = True
    
    # 