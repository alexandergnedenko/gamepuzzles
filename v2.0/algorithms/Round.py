import datetime
import random
import math

class Round:
    # Имя картинки
    picture = ""
    
    # Текущее расположение частичек
    slicesLocation = []
    
    # Время начала партии
    startTime = 0
    
    # Время конца партии
    endTime = 0
    
    # Потраченные за партию ходы
    spentedMoves = 0
        
    # Кодировка положеия частичек
    isOnBorder = []
    
    # Конструктор
    def __init__(self, picture):
        self.picture = picture
        
        self.slicesLocation = self.rightLocalization()
        self.isOnBorder = self.determineNull(self.slicesLocation)
        self.slicesLocation = self.testLocalization()
        
        self.startTime = datetime.datetime.now()
        
        self.spentedMoves = 0
    
    # Определяет положение пропуска относительно границ решетки
    # возвращает список [isOnTop, isOnDown, isOnRight, isOnLeft]
    def determineNull(self, slicesLocationCast):
        isOnTop = False
        isOnDown = False
        isOnRight = False
        isOnLeft = False
        
        for i in range(4):
            if (slicesLocationCast[0][i] == 0):
                isOnTop = True
                
        for i in range(4):
            if (slicesLocationCast[3][i] == 0):
                isOnDown = True
                
        for i in range(4):
            if (slicesLocationCast[i][3] == 0):
                isOnRight = True
        
        for i in range(4):
            if (slicesLocationCast[i][0] == 0):
                isOnLeft = True
        
        return [isOnTop, isOnDown, isOnRight, isOnLeft]
    
    # Тестовый метод
    # Создаёт "Почти правильное" расположение
    def testLocalization(self):
        ret = list()
        
        row = [1,2,3,4]        
        ret.append(row)
        
        row = [5,6,7,8]        
        ret.append(row)
        
        row = [9,10,11,12]        
        ret.append(row)
        
        row = [13,14,0,15]        
        ret.append(row)
        
        return ret
    
    # Создаёт список списков 4*4 с правильным заполнением числами от 0 до 16
    def rightLocalization(self):
        ret = list()
        
        row = [1,2,3,4]        
        ret.append(row)
        
        row = [5,6,7,8]        
        ret.append(row)
        
        row = [9,10,11,12]        
        ret.append(row)
        
        row = [13,14,15,0]        
        ret.append(row)
        
        return ret
    
    # Перетасовывает текущее slicesLocation k возможными ходами
    def randomizeLocalization(self, k=1000):
        if (self.slicesLocation == []):
            return "Error!"
        else:
            for i in range(k):
                move = random.randint(1, 4)
                if (move == 1):
                    self.moveUp()
                elif (move == 2):
                    self.moveDown()
                elif (move == 3):
                    self.moveRight()
                else:
                    self.moveLeft()
        return 
    
    # Движения пропуска вверх
    def moveUp(self):        
        if self.isOnBorder[0]:
            return False
        else:
            stoped = False
            
            for i in range(4):
                for j in range(4):
                    if ((self.slicesLocation[i][j] == 0)&(not stoped)):
                        self.slicesLocation[i][j] = self.slicesLocation[i-1][j]
                        self.slicesLocation[i-1][j] = 0
                        self.spentedMoves = self.spentedMoves + 1
                        
                        self.isOnBorder = self.determineNull(self.slicesLocation)
                        stoped = True
            return True
                        
    # Движения пропуска вниз
    def moveDown(self):        
        if self.isOnBorder[1]:
            return False
        else:
            stoped = False
            
            for i in range(4):
                for j in range(4):
                    if ((self.slicesLocation[i][j] == 0)&(not stoped)):
                        self.slicesLocation[i][j] = self.slicesLocation[i+1][j]
                        self.slicesLocation[i+1][j] = 0
                        self.spentedMoves = self.spentedMoves + 1
                        
                        self.isOnBorder = self.determineNull(self.slicesLocation)
                        stoped = True
            return True
                        
    # Движения пропуска вправо
    def moveRight(self):        
        if self.isOnBorder[2]:
            return False
        else:
            stoped = False
            
            for i in range(4):
                for j in range(4):
                    if ((self.slicesLocation[i][j] == 0)&(not stoped)):
                        self.slicesLocation[i][j] = self.slicesLocation[i][j+1]
                        self.slicesLocation[i][j+1] = 0
                        self.spentedMoves = self.spentedMoves + 1
                        
                        self.isOnBorder = self.determineNull(self.slicesLocation)
                        stoped = True
            return True            
                        
    # Движения пропуска влево
    def moveLeft(self):        
        if self.isOnBorder[3]:
            return False
        else:
            stoped = False
            
            for i in range(4):
                for j in range(4):
                    if ((self.slicesLocation[i][j] == 0)&(not stoped)):
                        self.slicesLocation[i][j] = self.slicesLocation[i][j-1]
                        self.slicesLocation[i][j-1] = 0
                        self.spentedMoves = self.spentedMoves + 1
                        
                        self.isOnBorder = self.determineNull(self.slicesLocation)
                        stoped = True
            return True
        
    # Метод проверки на правильность тукещего положения частичек
    def isCorrectOrder(self):
        correctOrder = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
        if (self.slicesLocation==correctOrder):
            isCorrect = True
        else:
            isCorrect = False
        return isCorrect
    
    # Метод завершения раунда
    def showInfoEndClose(self):
        self.endTime = datetime.datetime.now()
        spentTime = self.endTime - self.startTime
        minutes = math.floor(spentTime.total_seconds()//60)
        seconds = math.floor(spentTime.total_seconds()-60*minutes)
        
        spentMoves = self.spentedMoves
        
        return spentTime, spentMoves